#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/bootdevice/by-name/recovery:67108864:1da6cd4a824ba39fa69297b245b5d5b9ad5210c7; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/bootdevice/by-name/boot:67108864:98aaae37b08e0371eda203b5ec4945932cf39809 \
          --target EMMC:/dev/block/platform/bootdevice/by-name/recovery:67108864:1da6cd4a824ba39fa69297b245b5d5b9ad5210c7 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
